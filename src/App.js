import {useState} from 'react'

//import app components
import Home from 'pages/home'
import NavBar from 'components/AppNavBar'
import Register from 'pages/register'
import Login from 'pages/login'
import Logout from 'pages/logout'
import Categories from 'pages/categories'
import Entries from 'pages/entries'
import Expenses from 'pages/expenses'
import Income from 'pages/income'

//import CSS
import './App.css'

//import react-router-dom components
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

//import context provider
import {UserProvider} from './userContext'

//import react-bootstrap components
import {Container} from 'react-bootstrap'

function App() {

  const [user,setUser] = useState({

    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === "true"
  })

  console.log(user)

  const unsetUser = () => {
  localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <NavBar />
          <Container>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/categories" component={Categories}/>
              <Route exact path="/entries" component={Entries}/>
              <Route exact path="/entries/expenses" component={Expenses}/>
              <Route exact path="/entries/income" component={Income}/>
              <Route exact path="/register" component={Register}/>
              <Route exact path="/login" component={Login}/>
              <Route exact path="/logout" component={Logout}/>
            </Switch>
          </Container>
        </Router>
      </UserProvider>

    </>
  );
}

export default App;