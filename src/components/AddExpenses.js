import React,{useState,useEffect,useContext} from 'react'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row,Col,Jumbotron,Form,Button,Container,InputGroup} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function AddExpenses(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [addCategory,setAddCategory] = useState("")
	const [addAmount,setAddAmount] = useState("")

	const [allCategories,setAllCategories] = useState([])


	function addEntry(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/entries/',{

			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				category: addCategory,
				amount: addAmount,
				type: "expense"
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			Swal.fire({

				icon: "success",
				title: "Successfully added an expense entry!",
				text: "You have added an expense entry!"

  			}).then((result) => {
  			window.location.reload()
			})
			
		})

	}


	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/categories/',{
			
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			let expenseType = data.filter(type => {
				return type.type === "expense"
			})

			console.log(expenseType)

				setAllCategories(expenseType.map(category => {
		    	return (
					<option>{category.name}</option>
		    	)
				}))

		})

	},[])
		console.log(allCategories)


		return (
		<>
		<Col xs={12} md={6}>
		<Jumbotron className="mt-5" id="jumbo">
			<h3 className="text-center">Add an Entry</h3>
			<Container>
			<Row>
				<Col md={{ span: 10, offset: 1 }}>
				<Form onSubmit={e => addEntry(e)}>
					<Form.Group controlId="entryCategory">
					    <Form.Label>Category</Form.Label>
					    <Form.Control as="select" value={addCategory} onChange={(e)=>setAddCategory(e.target.value)} required>
					    	<option value="" selected>Select category..</option>
					    	{allCategories}
					    </Form.Control>
					</Form.Group>
					<Form.Group controlId="entryAmount">
						<Form.Label>
							Amount
						</Form.Label>
						    <InputGroup>
						        <InputGroup.Prepend>
						        	<InputGroup.Text>₱</InputGroup.Text>
						        </InputGroup.Prepend>
						        <Form.Control type="number" value={addAmount} onChange={(e)=>setAddAmount(e.target.value)} required />
						    </InputGroup>
					</Form.Group>
						<Button variant="info" type="submit">Submit</Button>
				</Form>
				</Col>
			</Row>
			</Container>
		</Jumbotron>
		</Col>
		</>

		)
}