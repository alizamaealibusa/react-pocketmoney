import React,{useState,useEffect,useContext} from 'react'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Col,Table,Jumbotron,Button} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function AllIncome(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [allEntries,setAllEntries] = useState([])
	const [allCategories,setAllCategories] = useState([])
	const [totalIncome,setTotalIncome] = useState([])


	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/entries/income',{
			
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllEntries(data.map(entry => {

			function deleteEntry(e){

				e.preventDefault()

				let token = localStorage.getItem('token')

				fetch(`https://gentle-retreat-12766.herokuapp.com/api/entries/${entry._id}`,{

					method: 'DELETE',
					headers: {
						Authorization: `Bearer ${token}`,
						'Content-Type': 'application/json'
					}

				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					Swal.fire({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, delete it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire({
							icon: "success",
							title: "Successfully deleted an income entry!",
							text: "You have deleted an income entry!"
  						}).then((result) => {
  							window.location.reload()
						})
					  }
					})
					
				})

			}

			function editEntry(e){

				e.preventDefault()

				let token = localStorage.getItem('token')

				Swal.fire({
				  title: 'Edit Expense',
				  html: `<input type="text" id="editCategory" class="swal2-input" placeholder="${entry.category}" disabled>
				  <input type="text" id="editAmount" class="swal2-input" placeholder="Amount">
				  <input type="text" id="editType" class="swal2-input" placeholder="${entry.type}" disabled>`,
				  confirmButtonText: 'Save',
				  allowOutsideClick: false,
				  allowEscapeKey: false,
				  preConfirm: () => {
				    const editAmount = Swal.getPopup().querySelector('#editAmount').value
				    if (!editAmount) {
				      Swal.showValidationMessage(`Please enter amount and type.`)
				    }
				    return { editAmount: editAmount }
				  }
				}).then((result) => {

					console.log(result)

				fetch(`https://gentle-retreat-12766.herokuapp.com/api/entries/${entry._id}`,{

					method: 'PUT',
					headers: {
						Authorization: `Bearer ${token}`,
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						category: entry.category,
						amount: result.value.editAmount,
						type:  entry.type
					})

				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					Swal.fire({

						icon: "success",
						title: "Successfully edited an income entry!",
						text: "You have edited an income entry!"

		  			}).then((result) => {
		  			window.location.reload()
					})
					
				})

				})

			}


	    	return (
				<tr className="green">
					<td>{entry.category}</td>
					<td>{entry.amount}</td>
					<td>{entry.type}</td>
					<td><Button variant="success" size="sm" value={entry._id} onClick={e => editEntry(e)}>Edit</Button> <Button variant="danger" size="sm" value={entry._id} onClick={e => deleteEntry(e)}>Delete</Button></td>
				</tr>
	    	)
			}))

			let amountArr = data.map(element => {
				return element.amount
			})

			if (amountArr.length >= 1) {
				console.log(amountArr)
				let total = amountArr.reduce((x,y) => x+y)
				console.log(total)
				setTotalIncome(total)
			}
		})

	},[])
		console.log(allEntries)


	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/categories/',{
			
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			let incomeType = data.filter(type => {
				return type.type === "income"
			})

			console.log(incomeType)

				setAllCategories(incomeType.map(category => {
		    	return (
					<option>{category.name}</option>
		    	)
				}))

		})

	},[])
		console.log(allCategories)


	return (
		<>
		<Col xs={12} md={6}>
		<Jumbotron className="mt-5" id="jumbo">
			<h3 className="text-center">Income</h3>
			<Table className="text-center" size="sm" responsive="sm" >
				<thead >
				<tr>
					<td>Category</td>
					<td>Amount</td>
					<td>Type</td>
					<td>Action</td>
				</tr>
				</thead>
				<tbody>
					{allEntries}
				<tr>
					<td><b>Total</b></td>
					<td><b>{totalIncome}</b></td>
					<td></td>
				</tr>
				</tbody>
			</Table>
		</Jumbotron>
		</Col>
		</>

			)
}