import React from 'react'

//react-bootstrap components
import {Row,Col,Card} from 'react-bootstrap'

export default function Highlights(){

	return (

		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h4>Easy tracking of your income and expenses.</h4>
						</Card.Title>
						<Card.Text>
							Stay on top of your finances by seeing where your money comes and goes.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h4>Plan your budget ahead of time.</h4>
						</Card.Title>
						<Card.Text>
							Plan your expenditures ahead of time, with less time, less worry!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h4>Stay focused on your financial goals.</h4>
						</Card.Title>
						<Card.Text>
							Improve your spending habits with goals that keep you going. Save for a home, eliminate debts, and prepare for the future.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)

}
