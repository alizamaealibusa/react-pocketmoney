import React,{useState,useContext} from 'react'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row,Col,Jumbotron,Form,Button,Container} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function AddCategory(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [addName,setAddName] = useState("")
	const [addType,setAddType] = useState("")


	function addCategory(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/categories/',{

			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: addName,
				type: addType
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (data._id) {
			Swal.fire({

				icon: "success",
				title: "Successfully added a category!",
				text: "You have added a category!"

  			}).then((result) => {
  				window.location.reload()
			})

	  		} else {
	  			Swal.fire({
					icon: "error",
					title: "Category already exists.",
					text: "Please create a new category or edit a given category."
				})
	  		}
			
		})

	}

		return (
		<>
		<Col xs={12} md={6}>
		<Jumbotron className="mt-5" id="jumbo">
			<h3 className="text-center">Add a Category</h3>
			<Container>
			<Row>
				<Col md={{ span: 10, offset: 1 }}>
				<Form onSubmit={e => addCategory(e)}>
					<Form.Group controlId="categoryName">
						<Form.Label>
							Name
						</Form.Label>
						<Form.Control type="text" value={addName} onChange={(e)=>setAddName(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="categoryType">
					    <Form.Label>Type</Form.Label>
					    <Form.Control as="select" value={addType} onChange={(e)=>setAddType(e.target.value)} required>
					    	<option value="" selected>Select type..</option>
					    	<option value="income">Income</option>
					    	<option value="expense">Expense</option>
					    </Form.Control>
					</Form.Group>
						<Button variant="info" type="submit">Submit</Button>
				</Form>
				</Col>
			</Row>
			</Container>
		</Jumbotron>
		</Col>
		</>
			
		)
}