import React,{useContext} from 'react'

//react-router-dom components: Link and NavLink.
import {Link,NavLink} from 'react-router-dom'

//import user context
import UserContext from 'userContext'

//react-bootstrap components
import {Navbar,Nav,NavDropdown} from 'react-bootstrap'

export default function NavBar(){

	const {user} = useContext(UserContext)
	console.log(user)

	return (

			<Navbar expand="lg" className="navbg">
				<Navbar.Brand as={Link} to="/">Pocket Money</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					{
						user.email
						?
						<>
							<Nav.Link as={NavLink} to="/categories" className="link-text">Categories</Nav.Link>
							<NavDropdown title="Entries" id="basic-nav-dropdown">
						        <NavDropdown.Item><Nav.Link as={NavLink} to="/entries/income" className="link-text">Income</Nav.Link></NavDropdown.Item>
						        <NavDropdown.Item><Nav.Link as={NavLink} to="/entries/expenses" className="link-text">Expenses</Nav.Link></NavDropdown.Item>
						        <NavDropdown.Divider />
						        <NavDropdown.Item><Nav.Link as={NavLink} to="/entries/" className="link-text">View All Entries</Nav.Link></NavDropdown.Item>
						    </NavDropdown>
						    <Nav className="ml-auto">
							<Nav.Link as={NavLink} to="/logout" className="link-text">Logout</Nav.Link>
							</Nav>
						</>
						:
						<>
							<Nav className="ml-auto">
							<Nav.Link as={NavLink} to="/login" className="link-text">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register" className="link-text">Register</Nav.Link>
							</Nav>
						</>
					}
				</Navbar.Collapse>

			</Navbar>

		)

}
