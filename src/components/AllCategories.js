import React,{useState,useEffect,useContext} from 'react'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Col,Table,Jumbotron,Button} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function AllCategory(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [allCategories,setAllCategories] = useState([])


	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/categories/',{
			
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCategories(data.map(category => {

			function deleteCategory(e){

				e.preventDefault()

				let token = localStorage.getItem('token')

				fetch(`https://gentle-retreat-12766.herokuapp.com/api/categories/${category._id}`,{

					method: 'DELETE',
					headers: {
						Authorization: `Bearer ${token}`,
						'Content-Type': 'application/json'
					}

				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					Swal.fire({
					  title: 'Are you sure?',
					  text: "You won't be able to revert this!",
					  icon: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, delete it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire({
							icon: "success",
							title: "Successfully deleted a category!",
							text: "You have deleted a category!"
  						}).then((result) => {
  							window.location.reload()
						})
					  }
					})
					
				})

			}

			function editCategory(e){

				e.preventDefault()

				let token = localStorage.getItem('token')

				Swal.fire({
				  title: 'Edit Category',
				  html: `<input type="text" id="editName" class="swal2-input" placeholder="${category.name}" disabled>
				  		<select class="swal2-input" id="editType">
				  		<option value="" selected>Select type..</option>
			          	<option value="income">Income</option>
			          	<option value="expense">Expense</option>
			          	</select>`,
				  confirmButtonText: 'Save',
				  allowOutsideClick: false,
				  allowEscapeKey: false,
				  preConfirm: () => {
				    const editType = Swal.getPopup().querySelector('#editType').value
				    if (!editType) {
				      Swal.showValidationMessage(`Please select category type.`)
				    }
				    return { editType: editType }
				  }
				}).then((result) => {

					console.log(result)

				fetch(`https://gentle-retreat-12766.herokuapp.com/api/categories/${category._id}`,{

					method: 'PUT',
					headers: {
						Authorization: `Bearer ${token}`,
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: category.name,
						type: result.value.editType
					})

				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					Swal.fire({

						icon: "success",
						title: "Successfully edited a category!",
						text: "You have edited a category!"

		  			}).then((result) => {
		  			window.location.reload()
					})
					
				})
				
				})

			}

			if (category.type === "income"){
				return (
				<tr className="green" key="category-income">
					<td>{category.name}</td>
					<td>{category.type}</td>
					<td><Button variant="success" size="sm" value={category._id} onClick={e => editCategory(e)}>Edit</Button> <Button variant="danger" size="sm" value={category._id} onClick={e => deleteCategory(e)}>Delete</Button></td>
				</tr>					
				)
			}
			if (category.type === "expense"){
				return (
				<tr className="red" key="category-expense">
					<td>{category.name}</td>
					<td>{category.type}</td>
					<td><Button variant="success" size="sm" value={category._id} onClick={e => editCategory(e)}>Edit</Button> <Button variant="danger" size="sm" value={category._id} onClick={e => deleteCategory(e)}>Delete</Button></td>
				</tr>
				)

			}

			}))

		})

	},[])
		console.log(allCategories)

		return (

					<Col xs={12} md={6}>
					<Jumbotron className="mt-5" id="jumbo">
						<h3 className="text-center">Categories</h3>
						<Table className="text-center" size="sm" responsive="sm" >
							<thead >
							<tr>
								<td>Name</td>
								<td>Type</td>
								<td></td>
							</tr>
							</thead>
							<tbody>
								{allCategories}
							</tbody>
						</Table>
					</Jumbotron>
					</Col>

			)
}