import React from 'react'

//react-bootstrap components
import {Jumbotron,Row,Col} from 'react-bootstrap'

export default function Banner({dataProp}){

	const {title,description} =  dataProp

	return (
		<>
			<Row>
				<Col>
					<Jumbotron className="mt-5" id="jumbo">
						<h1>{title}</h1>
						<p>{description}</p>
					</Jumbotron>
				</Col>
			</Row>
		</>

		)

}
