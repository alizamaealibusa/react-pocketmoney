import React, {useState,useContext} from 'react'

//import redirect component from react-router-dom
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//react-bootstrap components
import {Row, Jumbotron, Container, Form, Button, Col} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function Register(){

	//unwrap the UserContext to get the global user state and its setter function
	const {user} = useContext(UserContext)
	console.log(user)

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	//state for redirecting our user
	const [willRedirect,setWillRedirect] = useState(false)

	//submit function registerUser
	function registerUser(e){

		e.preventDefault()

		fetch('https://gentle-retreat-12766.herokuapp.com/api/users/',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword

			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

		if (data.email && (firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (mobileNo.length === 11) && (password.length >= 8) && (password === confirmPassword)){

			Swal.fire({

				icon: 'success',
				title: "Successful Registration!",
				text:"Thank you for registering."
			})
			setWillRedirect(true)

		} if (email !== data.email) {

			Swal.fire({

				icon: 'error',
				title: "Email already exists!",
				text:"Please use another email address."
			})

		} if (password !== confirmPassword) {

			Swal.fire({

				icon: 'error',
				title: "Passwords does not match.",
				text:"Please try re-entering passwords."
			})

		} if (mobileNo.length !== 11) {

			Swal.fire({

				icon: 'error',
				title: "Invalid mobile number",
				text:"Please input 11-digit mobile number."
			})

		} if ((password.length < 8) && (password !== confirmPassword)) {

			Swal.fire({

				icon: 'error',
				title: "Passwords does not match and too short.",
				text:"Please try re-entering passwords."
			})
				
		} if (password.length < 8) {

			Swal.fire({

				icon: 'error',
				title: "Password too short.",
				text:"Password must be 8 or more characters long."
			})
			
		}

		})

	}

	return (
	user.email
    ?
    < Redirect to="/entries" />
    :
		willRedirect
		?
		<Redirect to="/login"/>
		:
		<div>
		<Jumbotron className="mt-4" id="jumbo">
			<h3 className="text-center mb-3">Register</h3>
			<Container>
				<Row>
					<Col md={{ span: 6, offset: 3 }}>
					<Form onSubmit={e => registerUser(e)}>
						<Form.Row>
							<Form.Group as={Col} controlId="formGridFirstName">
								<Form.Label>First Name</Form.Label>
								<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required/>
							</Form.Group>
							<Form.Group as={Col} controlId="formGridLastName">
								<Form.Label>Last Name</Form.Label>
								<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required/>
							</Form.Group>
						</Form.Row>
						<Form.Group>
							<Form.Label>Email</Form.Label>
							<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}}required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control type="number" placeholder="Enter 11-digit Mobile Number" value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} required/>
						</Form.Group>
						<Form.Row>
							<Form.Group as={Col} controlId="formGridPassword">
								<Form.Label>Password</Form.Label>
								<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}}required/>
							</Form.Group>
							<Form.Group as={Col} controlId="formGridConfirmPassword">
								<Form.Label>Confirm Password</Form.Label>
								<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}}required/>
							</Form.Group>
						</Form.Row>
							<Button variant="info" type="submit">Submit</Button>
					</Form>
					</Col>
				</Row>
			</Container>
		</Jumbotron>
		</div>
		

		)

}
