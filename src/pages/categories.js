import React,{useContext} from 'react'

//import app components
import AllCategories from 'components/AllCategories'
import AddCategory from 'components/AddCategory'

//import react-router-dom components
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row} from 'react-bootstrap'



export default function Category(){

	const {user} = useContext(UserContext)
	console.log(user)


		return (
		user.email
        ?
		<>
			<Row>
				<AddCategory />
				<AllCategories />
			</Row>
		</>
		:
        < Redirect to="/login" />
		)
}