import React,{useContext} from 'react'

//import app components
import Banner from 'components/Banner'
import Highlights from 'components/Highlights'

//import the user context
import UserContext from 'userContext'

export default function Home(){

	const {user} = useContext(UserContext)
	console.log(user)

	let myTrackingApp = {
	  title: "Pocket Money",
	  description: "Smart, simple, online accounting software for tracking your income and expenses.",
	}


	return (

		<>
			<Banner dataProp={myTrackingApp} />
			<Highlights />
		</>

		)

}
