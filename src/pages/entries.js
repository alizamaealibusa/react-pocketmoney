import React,{useState,useEffect,useContext} from 'react'

//import react-router-dom components
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row,Col,Table,Jumbotron} from 'react-bootstrap'

export default function Entries(){

	const {user} = useContext(UserContext)
	console.log(user)

	const [allEntries,setAllEntries] = useState([])
	const [totalAmount,setTotalAmount] = useState([])


	useEffect(()=>{
		let token = localStorage.getItem('token')

		fetch('https://gentle-retreat-12766.herokuapp.com/api/entries/',{
			
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllEntries(data.map(entry => {

				if (entry.type === "income"){
					return (
					<tr className="green">
						<td>{entry.category}</td>
						<td>{entry.amount}</td>
						<td>{entry.type}</td>
					</tr>					
					)
				}
				if (entry.type === "expense"){
					return (
					<tr className="red">
						<td>{entry.category}</td>
						<td>{entry.amount}</td>
						<td>{entry.type}</td>
					</tr>
					)
				}

			}))


			let amountArr = data.map(element => {

				if (element.type === "expense") return element.amount * -1 
				if (element.type === "income") return element.amount * 1
					return element.amount
			})
			if (amountArr.length >= 1) {
				console.log(amountArr)
				let total = amountArr.reduce((x,y) => x+y)
				console.log(total)
				setTotalAmount(total)
			}
			return
		})

	},[])
		console.log(allEntries)


		return (
		user.email
        ?
		<>
			<Row>
				<Col md={{ span: 6, offset: 3 }}>
				<Jumbotron className="mt-5" id="jumbo">
					<h3 className="text-center">Entries</h3>
				<Table className="text-center" size="sm" responsive="sm" >
					<thead >
					<tr>
						<td>Category</td>
						<td>Amount</td>
						<td>Type</td>
					</tr>
					</thead>
					<tbody>
						{allEntries}
						<tr>
							<td><b>Running balance</b></td>
							<td><b>{totalAmount}</b></td>
							<td></td>
						</tr>
					</tbody>
				</Table>
				</Jumbotron>
				</Col>
			</Row>
		</>
		:
        < Redirect to="/login" />

		)
}