import React,{useContext} from 'react'

//import redirect component from react-router-dom
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import Swal
import Swal from 'sweetalert2'

export default function Logout() {

	const {user,setUser,unsetUser} = useContext(UserContext)
	console.log(user)

	if (user.email){
		unsetUser()

		Swal.fire({
			icon: "success",
			title: "Logged out successfully!",
			text: "Thank you for using Pocket Money! See you again soon!"
		})

		setUser({
			email: null,
			isAdmin: null
		})
	} else {
		setUser({
			email: null,
			isAdmin: null
		})

	}

	return (
	user.email
    ?
		<Redirect to ="/"/>
    :
		<Redirect to ="/login"/>
	)
}