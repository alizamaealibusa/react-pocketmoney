import React,{useContext} from 'react'

//import app components
import AddExpenses from 'components/AddExpenses'
import AllExpenses from 'components/AllExpenses'

//import react-router-dom components
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row} from 'react-bootstrap'



export default function Expenses(){

	const {user} = useContext(UserContext)
	console.log(user)


		return (
		user.email
        ?
		<>
			<Row>
				<AddExpenses />
				<AllExpenses />
			</Row>
		</>
		:
        < Redirect to="/login" />
		)
}