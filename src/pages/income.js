import React,{useContext} from 'react'

//import app components
import AddIncome from 'components/AddIncome'
import AllIncome from 'components/AllIncome'

//import react-router-dom components
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row} from 'react-bootstrap'



export default function Income(){

	const {user} = useContext(UserContext)
	console.log(user)


		return (
		user.email
        ?
		<>
			<Row>
				<AddIncome />
				<AllIncome />
			</Row>
		</>
		:
        < Redirect to="/login" />
		)
}