import React, {useState,useContext} from 'react'

//import redirect component from react-router-dom
import {Redirect} from 'react-router-dom'

//import the user context
import UserContext from 'userContext'

//import react-bootstrap components
import {Row, Jumbotron, Container, Form, Button, Col} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'



export default function Login(){

	//unwrap the UserContext to get the global user state and its setter function
	const {user,setUser} = useContext(UserContext)
	console.log(user)

	//States for Email and Password
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	//state for redirecting our user
	const [willRedirect,setWillRedirect] = useState(false)

	function loginUser(e){

		e.preventDefault()


		fetch('https://gentle-retreat-12766.herokuapp.com/api/users/login',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if ((data.accessToken) && (email !== "" && password !== "") && (password.length >= 8)) {
				localStorage.setItem('token',data.accessToken)			

				Swal.fire({
					icon: "success",
					title: "Logged in successfully!",
					text: "Thank you for logging in to Pocket Money! Let's start saving!"
				})
				//get user's details from our token
				fetch('https://gentle-retreat-12766.herokuapp.com/api/users/',{

					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem('email',data.email)
					localStorage.setItem('isAdmin',data.isAdmin)

					setWillRedirect(true)

					setUser({
						email: data.email,
						isAdmin: data.isAdmin
					})

				})

			} else {

				Swal.fire({
					icon: "error",
					title: "Login failed.",
					text: "Please input correct email and password."
				})
			}
			
			
		})

		setPassword("")
	
	}

	return (
	user.email
    ?
    < Redirect to="/entries" />
    :
		willRedirect
		?
		<Redirect to="/entries"/>
		:
		<div>
		<Jumbotron className="mt-4" id="jumbo">	
			<h3 className="text-center mb-3">Login</h3>
			<Container>
				<Row>
					<Col md={{ span: 6, offset: 3 }}>
					<Form onSubmit={e => loginUser(e)}>
						<Form.Group controlId="userEmail" >
							<Form.Label>
								Email
							</Form.Label>
							<Form.Control  type="email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
						</Form.Group>
						<Form.Group controlId="userPassword">
							<Form.Label>
								Password
							</Form.Label>
							<Form.Control type="password" value={password} onChange={(e)=>setPassword(e.target.value)}required />
						</Form.Group>
							<Button variant="info" type="submit">Submit</Button>
					</Form>
					</Col>
				</Row>
			</Container>
		</Jumbotron>
		</div>
		)

}
